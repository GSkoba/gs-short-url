package com.skobelev.app;

import org.springframework.validation.annotation.Validated;

import java.util.Collection;


@Validated
public interface UrlManager {
    public String getUrlByKey(String key);
    public String shortenUrl(String url);
    public Collection<String> listShortUrls();
}
