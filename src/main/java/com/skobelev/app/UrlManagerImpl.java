package com.skobelev.app;

import com.google.common.hash.Hashing;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class UrlManagerImpl implements UrlManager {

    private Map<String, Url> map = new HashMap<>();

    @Override
    public String getUrlByKey(String key) {
        if (key == null) {
            throw new IllegalStateException("Key не может быть null");
        }
        Url url = map.get(key);
        return url.getUrl();
    }

    @Override
    public String shortenUrl(String url) {
        if (url == null || url.contains("@")) {
            throw new IllegalStateException("Url является null или имеет не валидный символ");
        }

        String key = Hashing.murmur3_32().hashString(url, Charset.defaultCharset()).toString();

        Url shortUrlEntry = new Url(key, url, LocalDateTime.now());

        map.put(key, shortUrlEntry);

        return key;
    }

    @Override
    public Collection<String> listShortUrls() {
        return map.keySet();
    }
}
