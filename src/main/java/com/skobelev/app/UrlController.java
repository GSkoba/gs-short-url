package com.skobelev.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping(value = "/gs")
public class UrlController {

    @Autowired
    private UrlManager urlManager;

    @RequestMapping(value = "/{url}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity shortenUrl(@PathVariable String url) {
        return ResponseEntity.ok(urlManager.shortenUrl(url));
    }

    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    @ResponseBody
    public RedirectView getUrl(@PathVariable String key) {
        String url = urlManager.getUrlByKey(key);
        return new RedirectView("https://" + url);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity listShortUrls() {
        return ResponseEntity.ok(urlManager.listShortUrls());
    }
}
