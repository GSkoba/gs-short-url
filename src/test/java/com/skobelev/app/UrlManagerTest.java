package com.skobelev.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UrlManagerTest {

    @Test
    @DisplayName("Тест на успешное создание короткой ссылки")
    public void successCreateShortUlr() {
        var urlManager = new UrlManagerImpl();
        String longUrl = "www.google.com";

        var actualShortUrl = urlManager.shortenUrl(longUrl);
        Assertions.assertNotNull(actualShortUrl);

        var actualLongUrl = urlManager.getUrlByKey(actualShortUrl);
        Assertions.assertNotNull(actualLongUrl);

        Assertions.assertEquals(longUrl, actualLongUrl);

        var urls = urlManager.listShortUrls();
        Assertions.assertNotNull(urls);
        Assertions.assertEquals(1, urls.size());
    }

    @Test
    @DisplayName("Тест на ошибку при не валидных значениях")
    public void throwErrorFroNotValidArguments() {
        var urlManager = new UrlManagerImpl();

        Assertions.assertThrows(IllegalStateException.class, () -> urlManager.shortenUrl(null));
        Assertions.assertThrows(IllegalStateException.class, () -> urlManager.shortenUrl("test@com"));
        Assertions.assertThrows(IllegalStateException.class, () -> urlManager.getUrlByKey(null));
    }

    @Test
    @DisplayName("Успешное создание множества коротких ссылок")
    public void successManyShortUrls() {
        var urlManager = new UrlManagerImpl();
        var longUrlFirst = "www.skillbox.ru";
        var longUrlSecond = "www.youtube.com";

        var shortUlrFirst = urlManager.shortenUrl(longUrlFirst);
        Assertions.assertNotNull(shortUlrFirst);
        var shortUrlSecond = urlManager.shortenUrl(longUrlSecond);
        Assertions.assertNotNull(shortUrlSecond);

        var actualLongUrlFirst = urlManager.getUrlByKey(shortUlrFirst);
        var actualLongUrlSecond = urlManager.getUrlByKey(shortUrlSecond);

        Assertions.assertEquals(longUrlFirst, actualLongUrlFirst);
        Assertions.assertEquals(longUrlSecond, actualLongUrlSecond);
    }
}
